#include <stdio.h>
#include <process_LHCb_raw_file.h>
#include <string.h> // for memcpy

extern "C" {
  bool print_event(struct LHCb_Raw_Event* event, void*) {
    printf("====================== Raw event content ===========================\n");
    print_LHCb_raw_event(event);
    printf("====================== End of event ===========================\n");
    return true;    
  }
  static bool print_bank(struct LHCb_Raw_Bank* bank, void*) {
    print_LHCb_raw_bank(bank);
    return true;
  }
}

int main(int argc, char** argv) {
  if (argc != 2) {
    printf("This program dumps raw LHCb events and their banks. Usage: %s <file_name>\n", argv[0]);
    return 1;
  }
  process_LHCb_raw_file(argv[1], &print_bank, NULL, &print_event, NULL, 0, 5);
  return 0;
}

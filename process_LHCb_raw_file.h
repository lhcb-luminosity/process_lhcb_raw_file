#ifndef process_LHCb_raw_file_h 
#define process_LHCb_raw_file_h 1

struct LHCb_Raw_File {
  unsigned char *start, *end;
};
struct LHCb_Raw_Event {
  unsigned char *start, *end;
};
struct LHCb_Raw_Bank {
  unsigned char *start, *end;
  unsigned char type, version;
  unsigned short source_ID;
};

// The following function processes events and banks in LHCb raw file
// "raw_file_name". It calls external functions *process_event and
// *process_bank for every event and bank, respectively. These
// functions should accept 1) the pointer to Event / Bank and other
// necessary arguments passed via void*. These other arguments
// themselves are specified (also as void* pointers) via event_arg and
// bank_arg. "max_event" defines how many events should be processed,
// 0 means all events. "debug_level">0 prints debugging information.
bool process_LHCb_raw_file(const char* raw_file_name,
			   bool (*process_bank )(struct LHCb_Raw_Bank* , void*), void* bank_arg,
			   bool (*process_event)(struct LHCb_Raw_Event*, void*), void* event_arg,
			   unsigned int max_events,
			   int debug_level);

// Almost the same as "process_LHCb_raw_file" above, but also prints
// to stdout the byte content of every bank (in decimal form), and of
// every event if print_events is true. This is done using the
// functions "print_LHCb_raw_event/bank()" below.
// "debug_level"=1 prints some debugging information, >=2 prints also
// the whole raw event in addition to raw banks.
bool print_and_process_LHCb_raw_file(const char* raw_file_name,
				     bool (*process_bank )(struct LHCb_Raw_Bank* , void*), void* bank_arg,
				     bool (*process_event)(struct LHCb_Raw_Event*, void*), void* event_arg,
				     unsigned int max_events,
				     int debug_level);
/*
  Usage example:

#include <process_LHCb_raw_file.h>
#include <HltLumiSummary_Decoder.hh>
#include <string.h> // for memcpy

bool get_lumi(LHCb_Raw_Bank* b, void* arg) {
  if (strcmp(LHCb_raw_bank_name(b), "HltLumiSummary") == 0 &&
      b->version == 2 &&
      b->source_ID == 256) {
    HltLumiSummary_Decoder* lumi = (HltLumiSummary_Decoder*) arg;
    return lumi->get(b->start, b->end, 1);
  } else return true;  
}

int main(int argc, char** argv) {
  if (argc != 2) {
    printf("Usage: %s <file_name>\n", argv[0]);
    exit(1);
  }
  HltLumiSummary_Decoder lumi;
  print_and_process_LHCb_raw_file(argv[1], NULL, NULL, &get_lumi, &lumi, 0, 0);
  return 0;
}
 */

void print_LHCb_raw_event(const struct LHCb_Raw_Event* e);
void print_LHCb_raw_bank(const struct LHCb_Raw_Bank* b);

// ------------------------------ Low level functions ------------------------------
// They can be used directly instead of process_LHCb_raw_file() above
//
// open / memory map file to a bytestream file->start ... file->end
bool open_LHCb_raw_file_mmap(const char* file_name, struct LHCb_Raw_File* file);
// read one event from start to maximally end_file, store output to &event
bool read_LHCb_raw_event(unsigned char* start, unsigned char* end_file, struct LHCb_Raw_Event* event);
// read one bank from start to maximally end_event, store output to &bank
bool read_LHCb_raw_bank(unsigned char* start, unsigned char* end_event, struct LHCb_Raw_Bank* bank);
// ----------------------------------------------------------------------------------

// One can select eg. HltLumiSummary banks as:
// if (raw_bank_name(&bank) == "HltLumiSummary").
// The raw bank names are taken from
// https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/DAQEvent/include/Event/RawBank.h#L62
const char* LHCb_raw_bank_name(const struct LHCb_Raw_Bank* bank);

#endif

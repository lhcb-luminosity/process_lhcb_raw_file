// To do:
// - change loop over bitset here & in ODIN to iterators
// - merge data and decoder
// - use print_counters
#ifndef HltLumiSummary_Decoder_hh 
#define HltLumiSummary_Decoder_hh 1

#include <ODIN_Decoder.hh>
#include <nlohmann_json.hpp>
#include <boost/dynamic_bitset.hpp>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <unistd.h> // for lseek
#include <errno.h>

#include <curl/curl.h>
#include <sys/stat.h> // for stat() checking json file emptiness

using namespace std;

typedef vector<long int> Event;
// not long long: limited by boost::dynamic_bitset<unsigned char>::to_ulong, ulonglong is not implemented

struct Events : public vector<Event> {
  void sort();
  bool print(FILE* f) const;
};

struct HltLumiSummary_Decoder {
  HltLumiSummary_Decoder(const char* output_file_name_prefix)
    : m_output_file_name_prefix(output_file_name_prefix) {}
  struct Field {
    const std::string& name() const { return m_name; }
    long int data() const { return m_data; }
  private:
    void change_scale() {
      // degrade (too fine) ECal binning by 100 and mention it in the name
      if (m_name.substr(0, 4) == "ECal") {
        scale *= 100;
        m_name += "/100";
      }
    }
    friend HltLumiSummary_Decoder;
    std::string m_name;
    unsigned int offset, size;
    bool shifted_scaled; // if true:
    long int shift;
    double scale;
    // using floor below instead of round as floor is better mimicking hardware ADC: [0..1)->0, [1..2)->2 etc.
    void shift_scale() { if (shifted_scaled) m_data = (long int)floor(((long int)m_data - shift) / scale); }
    bool debug_print() const;
    long int m_data; // not long long: limited by
    // boost::dynamic_bitset<unsigned char>::to_ulong, ulonglong is not implemented
  };
  struct Fields : public std::vector<Field> {
    Fields(const std::array<unsigned char, 4> schema, int debug_level = 0);
    ~Fields() {
      events.sort();
      if (!events.print(m_file)) {
        fprintf(stderr, "Can not save data to %s\n", m_file_name.c_str());
        return;
      }
      fclose(m_file);
      int ret = system(("bzip2 -f " + m_file_name).c_str()); // -f forces overwrite of output file
      printf("bzip2 return code: %i\n", ret);
    }
    FILE* file() const { return m_file; }
    void save();
    int nBytes() const { return m_nBytes; }
    int version() const { return m_version; }
  private:
    friend HltLumiSummary_Decoder;
    std::string m_file_name;
    FILE* m_file;
    Events events; // huge container keeping all lumi-data, to be sorted before writing to file
    boost::dynamic_bitset<unsigned char> bits; // copy of HltLumiSummary, contains constant
    // number of bits equal to HLTLumiSummary bank length for the given encoding schema
    int m_version, m_nBytes;
  };
  std::map<std::array<unsigned char, 4>, Fields> decoders; // decoders[schema] = Fields
  ODIN_Decoder odin_decoder; // complements lumi bank, for any schema
  bool get(unsigned char* start_block, unsigned char* end_block, int debug_level = 0);
  void save(); // copies ODIN content and last read lumi to "events" container
private:
  Fields* last_read; // pointer where last get is saved
  std::string m_output_file_name_prefix;
  struct Lumi_JSON_schema : public nlohmann::json {
    Lumi_JSON_schema(const std::array<unsigned char, 4> schema, int debug_level = 0);
  private:
    static bool download_json_schema(const char* file_name, int debug_level = 0); // creates (in init())
    // local json file if it does not exist or empty
    static size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream); // required for curl
  };
};

// ============================================================================
// =                            Implementation                                =
// ============================================================================
void Events::sort() { // by orbit+bcid
  ::sort(begin(), end(), [](const Event& a, const Event& b) {
    return a[ODIN_Decoder::ORBIT] < b[ODIN_Decoder::ORBIT] ||
      (a[ODIN_Decoder::ORBIT] == b[ODIN_Decoder::ORBIT] && a[ODIN_Decoder::BCID] < b[ODIN_Decoder::BCID]);
  });
}
bool Events::print(FILE* f) const {
  for (const_iterator it1 = begin(); it1 != end(); ++it1) {
    Event::const_iterator it2 = it1->begin();
    for (; it2!=(--it1->end()); ++it2) {
      if (fprintf(f, "%li,", *it2) <= 0) return false;
    }
    if (fprintf(f, "%li\n", *it2) <= 0) return false; // no last comma
  }
  return true;
}

inline HltLumiSummary_Decoder::Fields::Fields(const std::array<unsigned char, 4> schema, int debug_level) {
  if (debug_level > 0) {
    printf("4-bytes code of HltLumiSummary format (\"schema\") is %.2x%.2x%.2x%.2x\n",
           schema[0], schema[1], schema[2], schema[3]);
  }
  HltLumiSummary_Decoder::Lumi_JSON_schema j(schema);
  if (j.empty()) {
    fprintf(stderr, "No lumi-counters defined by encoding schema\n");
    return;
  }
  {
    const auto& j_counters = j["counters"];
    m_version = j["version"];
    m_nBytes = j["size"];
    for (const auto& [key, value] : j_counters.items()) {
      if (value["name"] != "encodingKey") { // do not include encodingKey as a counter
        push_back(Field());
        back().m_name = value["name"];
        back().offset = value["offset"];
        back().size = value["size"];
        back().shifted_scaled = value.contains("scale");
        if (back().shifted_scaled) {
          back().shift = value["shift"];
          back().scale = value["scale"];
          // degrade (too fine) ECal binning by 100:
          back().change_scale();
        }
      }
    }
    if (debug_level > 0) {
      printf("%lu counters:\n", size());
      for (const_iterator it=begin(); it!=end(); ++it) it->debug_print();
    }
  }
}

inline bool HltLumiSummary_Decoder::get(unsigned char* start_block, unsigned char* end_block, int debug_level) {
  int nBytes = end_block - start_block;
  if (nBytes < 4) {
    fprintf(stderr, "HltLumiSummary bank size is too short: %i, this is <4 bytes needed for decoding schema. Exiting\n",
            nBytes);
    return false;
  }
  std::array<unsigned char, 4> schema = {start_block[3], start_block[2], start_block[1], start_block[0]}; // reverse order
  auto [iter, new_schema] = decoders.try_emplace(schema, schema, debug_level);
  Fields& decoder = iter->second;
  if (new_schema) {
    if (decoder.empty()) return false;
    // write to a file: <file_name>_<encoding schema>
    char schema_str[10];
    sprintf(schema_str, "%.2x%.2x%.2x%.2x", schema[0], schema[1], schema[2], schema[3]);
    {
      decoder.m_file_name = m_output_file_name_prefix + "_" + schema_str;
      decoder.m_file = fopen(decoder.m_file_name.c_str(), "a+");
      if (! decoder.m_file ) {
        perror(("Cannot open file " + decoder.m_file_name).c_str());
        return false;
      }
      if (fgetc(decoder.m_file) == EOF) {
        // print csv header with ODIN added in front but only if file is empty, ie. does not have header
        fprintf(decoder.m_file, "%s,", ODIN_Decoder::field_names);
        for (Fields::const_iterator it=decoder.begin(); it!=(--decoder.end()); ++it)
          if (fprintf(decoder.m_file, "%s,", it->m_name.c_str()) <= 0) return false;
        if (fprintf(decoder.m_file, "%s\n", decoder.back().m_name.c_str()) <= 0) return false; // no last comma
      }
    }
  }
  // initialization done
  if (end_block - start_block != decoder.nBytes()) {
    fprintf(stderr, "Length of HltLumiSummary bank is %li but should be %i according to schema. Exiting\n",
            end_block - start_block, decoder.nBytes());
    return false;
  }
  decoder.bits = boost::dynamic_bitset<unsigned char>(start_block, end_block);
  if (debug_level > 2) { // print bit stream, keep reverse order like when printing directly cout << bits
    for (int i=decoder.bits.size()-1; i>=0; --i) {
      if (i%8==7) printf(" %i:", i+1);
      printf("%u", (unsigned int)(decoder.bits[i]));
    }
    printf("\n");
  }
  // Decoding part
  size_t bits_size = decoder.bits.size();
  for (Fields::iterator it=decoder.begin(); it!=decoder.end(); ++it) {
    unsigned int n_strip = bits_size - it->size; // = right offset + (left) offset
    // n_strip - offset         offset
    // ................ counter ......
    // shift left by n_strip - offset:
    // counter ......00000000000000000
    // then right by n_strip:
    // 00000000000000000000000 counter
    it->m_data = (long int) ((decoder.bits << (n_strip - it->offset)) >> n_strip).to_ulong();
    it->shift_scale(); // does nothing if not shifted/scaled
  }
  if (debug_level > 1) {
    for (Fields::const_iterator it=decoder.begin(); it!=decoder.end(); ++it)
      if (printf("%s = %lu\n", it->m_name.c_str(), it->m_data) <= 0) return false;
  }
  last_read = &decoder;
  return true;
}

inline bool HltLumiSummary_Decoder::Field::debug_print() const {
  if (shifted_scaled)
    return printf("%s: offset=%u, size=%u, shift=%li, scale=%g\n", m_name.c_str(), offset, size, shift, scale) > 0;
  else 
    return printf("%s: offset=%u, size=%u\n", m_name.c_str(), offset, size) > 0;
} 

inline size_t HltLumiSummary_Decoder::Lumi_JSON_schema::write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
  size_t written = fwrite(ptr, size, nmemb, stream);
  return written;
}

inline bool HltLumiSummary_Decoder::Lumi_JSON_schema::download_json_schema(const char* file_name, int debug_level) {
  //  curl_global_init(CURL_GLOBAL_ALL);
  CURL* curl = curl_easy_init();
  if (!curl) return false;
  std::string url =
    std::string("https://gitlab.cern.ch/lhcb-conddb/file-content-metadata/-/raw/master/luminosity_counters/json/")
    + std::string(file_name, 2) + "/" + file_name;
  // eg. https://gitlab.cern.ch/lhcb-conddb/file-content-metadata/-/raw/master/luminosity_counters/json/61/61967914.json
  curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
  /* tell libcurl to follow redirection */
  curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
  /* Switch on full protocol/debug output while testing */
  if (debug_level >= 2) curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
  FILE* output = fopen(file_name, "wb");
  if(output) {
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, output);
    curl_easy_perform(curl);
    fclose(output);
    if (debug_level >= 1) printf(">>>> JSON HltLumiSummary_Decoder schema is retrieved from \n>>>> %s\n>>>> and stored to %s\n",
                                 url.c_str(), file_name);
  } else return false;
  curl_easy_cleanup(curl);
  //curl_global_cleanup();
  return true;
} 

HltLumiSummary_Decoder::Lumi_JSON_schema::Lumi_JSON_schema(const std::array<unsigned char, 4> schema, int debug_level) {
  char json_file_name[2*4 + 5 + 1]; // "b1b2b3b4.json\0" where b1,b2,... are hexadecimal representations of bytes 4,3,2,1
  sprintf(json_file_name, "%.2x%.2x%.2x%.2x.json",schema[0],schema[1],schema[2],schema[3]);
  { // get json file from internet using curl only if it does not exist or empty:
    struct stat stat_record;
    if (stat(json_file_name, &stat_record) // !=0 if file does not exist
        || stat_record.st_size == 0) {
      if (!download_json_schema(json_file_name, debug_level)) {
        fprintf(stderr, "Can not get HltLumiSummary_Decoder format (\"schema\") from GitLab file name \"%s\". Exiting\n",
                json_file_name);
        return; // remains empty in case of any failure
      } else if (debug_level > 0) printf(">>>> JSON HltLumiSummary_Decoder schema is downloaded to %s\n", json_file_name);
    } else if (debug_level > 0) printf(">>>> JSON HltLumiSummary_Decoder schema is taken from %s\n", json_file_name);
  }
  std::ifstream i(json_file_name);
  if (!i) {
    fprintf(stderr, "Can not open %s\n", json_file_name);
    return;
  }
  nlohmann::json::operator=( nlohmann::json::parse(i) );
}

void HltLumiSummary_Decoder::save() {
  last_read->events.push_back(Event(ODIN_Decoder::N_FIELDS + last_read->size()));
  Event::iterator it = last_read->events.back().begin();
  for (ODIN_Decoder::Fields::const_iterator it_odin=odin_decoder.decoder().begin();
       it_odin!=odin_decoder.decoder().end(); ++it_odin, ++it)
    *it = it_odin->data();
  for (Fields::const_iterator it_lumi = last_read->begin(); it_lumi!=last_read->end(); ++it_lumi, ++it)
    *it = it_lumi->m_data;
}

#endif

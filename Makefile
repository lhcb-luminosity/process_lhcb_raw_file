ARCH := $(shell uname -m)

# Default Makefile shll is sh, which does not implement source command needed
# for setting up the environment. Use bash instead:
SHELL := $(shell which bash)

# Include BOOST if it is not included by default:
# check whether BOOST_DIR below exists (in lxplus or in grid)
BOOST_DIR := /cvmfs/sft.cern.ch/lcg/releases/LCG_103/Boost/1.81.0/$(ARCH)-centos9-gcc11-opt
ifneq ("$(wildcard $(BOOST_DIR))","")  
  # C++ boost is needed only for dynamic_bitset class.
  # It does not matter which LCG architecture of Boost to use, as it is template-based.
  # Any version with dynamic_boost should be fine.
  # To be explicit and do not depend on any external settings, use this one
  include := -I$(BOOST_DIR)
else
  include :=
endif

# check if g++ supports C++17, ie. its major version >=8, otherwise take gcc
# 12.1.0 (eg. in lxplus or grid):
GCC_VERSION_LT_8 := $(shell expr `gcc -dumpversion | cut -f1 -d.` \< 8)
ifeq ($(GCC_VERSION_LT_8), 1)
  SETENV_FILE := /cvmfs/lhcb.cern.ch/lib/lcg/releases/gcc/12.1.0/$(ARCH)-centos7/setup.sh
  SETENV := source $(SETENV_FILE) &&
else
  SETENV := 
endif

CPPFLAGS := -pedantic -Wall -Wextra -Wshadow -Wcast-align=strict -Wcast-qual \
            -Wformat=2 -Wlogical-op -Wredundant-decls -Wdisabled-optimization
# After the following, "make print-ARCH" or "make print-<Any_variable>" will print any Makefile variable
print-%  : ; @echo $* = $($*)

lumi2csv : lumi2csv.C HltLumiSummary_Decoder.hh ODIN_Decoder.hh \
           process_LHCb_raw_file.h \
           process_LHCb_raw_file.c nlohmann_json.hpp
	$(SETENV) g++ $(CPPFLAGS) -I. -Ofast $(include) lumi2csv.C process_LHCb_raw_file.c -lcurl -std=c++17 -o lumi2csv
dump_all_events : dump_all_events.C process_LHCb_raw_file.h process_LHCb_raw_file.c
	$(SETENV) g++ $(CPPFLAGS) -I. $(include) dump_all_events.C process_LHCb_raw_file.c -std=c++17 -o dump_all_events
process_lumi : process_lumi.C HltLumiSummary_Decoder.hh ODIN_Decoder.hh process_LHCb_raw_file.h \
               process_LHCb_raw_file.c nlohmann_json.hpp
	$(SETENV) g++ $(CPPFLAGS) -I. $(include) process_lumi.C process_LHCb_raw_file.c -lcurl -std=c++17 -o process_lumi

#include <process_LHCb_raw_file.h>

#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <error.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>

const char* LHCb_raw_bank_name(const struct LHCb_Raw_Bank* bank) {
  static const char* bank_type[] = { // from https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/DAQEvent/include/Event/RawBank.h#L62
    "L0Calo",                    //  0
    "L0DU",                      //  1
    "PrsE",                      //  2
    "EcalE",                     //  3
    "HcalE",                     //  4
    "PrsTrig",                   //  5
    "EcalTrig",                  //  6
    "HcalTrig",                  //  7
    "Velo",                      //  8
    "Rich",                      //  9
    "TT",                        // 10
    "IT",                        // 11
    "OT",                        // 12
    "Muon",                      // 13
    "L0PU",                      // 14
    "DAQ",                       // 15
    "ODIN",                      // 16
    "HltDecReports",             // 17
    "VeloFull",                  // 18
    "TTFull",                    // 19
    "ITFull",                    // 20
    "EcalPacked",                // 21
    "HcalPacked",                // 22
    "PrsPacked",                 // 23
    "L0Muon",                    // 24
    "ITError",                   // 25
    "TTError",                   // 26
    "ITPedestal",                // 27
    "TTPedestal",                // 28
    "VeloError",                 // 29
    "VeloPedestal",              // 30
    "VeloProcFull",              // 31
    "OTRaw",                     // 32
    "OTError",                   // 33
    "EcalPackedError",           // 34
    "HcalPackedError",           // 35
    "PrsPackedError",            // 36
    "L0CaloFull",                // 37
    "L0CaloError",               // 38
    "L0MuonCtrlAll",             // 39
    "L0MuonProcCand",            // 40
    "L0MuonProcData",            // 41
    "L0MuonRaw",                 // 42
    "L0MuonError",               // 43
    "GaudiSerialize",            // 44
    "GaudiHeader",               // 45
    "TTProcFull",                // 46
    "ITProcFull",                // 47
    "TAEHeader",                 // 48
    "MuonFull",                  // 49
    "MuonError",                 // 50
    "TestDet",                   // 51
    "L0DUError",                 // 52
    "HltRoutingBits",            // 53
    "HltSelReports",             // 54
    "HltVertexReports",          // 55
    "HltLumiSummary",            // 56
    "L0PUFull",                  // 57
    "L0PUError",                 // 58
    "DstBank",                   // 59
    "DstData",                   // 60
    "DstAddress",                // 61
    "FileID",                    // 62
    "VP",                        // 63
    "FTCluster",                 // 64
    "VL",                        // 65
    "UT",                        // 66
    "UTFull",                    // 67
    "UTError",                   // 68
    "UTPedestal",                // 69
    "HC",                        // 70
    "HltTrackReports",           // 71
    "HCError",                   // 72
    "VPRetinaCluster",           // 73
    "FTGeneric",                 // 74
    "FTCalibration",             // 75
    "FTNZS",                     // 76
    "Calo",                      // 77
    "CaloError",                 // 78
    "MuonSpecial",               // 79
    "RichCommissioning",         // 80
    "RichError",                 // 81
    "FTSpecial",                 // 82
    "CaloSpecial",               // 83
    "Plume",                     // 84
    "PlumeSpecial",              // 85
    "PlumeError",                // 86
    "VeloThresholdScan",         // 87
    "FTError",                   // 88
    "DaqErrorFragmentThrottled", // 89
    "DaqErrorBXIDCorrupted",     // 90
    "DaqErrorSyncBXIDCorrupted", // 91
    "DaqErrorFragmentMissing",   // 92
    "DaqErrorFragmentTruncated", // 93
    "DaqErrorIdleBXIDCorrupted", // 94
    "DaqErrorFragmentMalformed", // 95
    "DaqErrorEVIDJumped",        // 96
    "VeloSPPandCluster"          // 97
    };
  static unsigned int n_banks = sizeof(bank_type) / sizeof(bank_type[0]);
  static const char* unknown = "UNKNOWN";
  return (bank->type < n_banks) ? bank_type[bank->type] : unknown;
}

void print_LHCb_raw_event(const struct LHCb_Raw_Event* e) {
  printf("Event size = %li bytes\n", e->end - e->start);
  for (unsigned char* p=e->start; p!=e->end; ++p) printf("%i ", *p);
  printf("\n");
}

void print_LHCb_raw_bank(const struct LHCb_Raw_Bank* b) {
  printf("Bank type = %i (%s), version = %i, source ID = %i, N bytes = %li\n", b->type, LHCb_raw_bank_name(b),
	 b->version, b->source_ID, b->end - b->start);
  for (unsigned char* p=b->start; p!=b->end; ++p) printf("%i ", *p);
  printf("\n");
}

static inline unsigned int read_length(unsigned char start[4]) {
  return start[0] + ((unsigned int)start[1] << 8) + ((unsigned int)start[2] << 16) + ((unsigned int)start[3] << 24);
}

// read one event from start to maximally end_file, store output to &event
bool read_LHCb_raw_event(unsigned char* start, unsigned char* end_file, struct LHCb_Raw_Event* event) {
  unsigned int n;
  if (start + 48 >= end_file) {
    fprintf(stderr, "file ends before event finishes: %ti bytes remain in file <= 48 = event header size\n",
            end_file - start);
    return false;
  }
  unsigned int n1 = read_length(start);
  unsigned int n2 = read_length(start + 4);
  unsigned int n3 = read_length(start + 8);
  if      (n1 == n2) n = n1;
  else if (n1 == n3) n = n1;
  else if (n2 == n3) n = n2;
  else {
    fprintf(stderr, "All 3 event lengths in event header are different: %u != %u != %u\n", n1, n2, n3);
    return false;
  }
  if (n <= 48) {
    fprintf(stderr, "event length is too short: %u bytes, while it must contain first 48 bytes for header\n",
            n);
    return false;
  }
  event->start = start + 48;
  event->end   = start + n;
  if (event->end > end_file) {
    fprintf(stderr, "file ends before event finishes: %ti bytes remain in file < %u = requested event size\n",
            end_file - start, n);
    return false;
  }
  //  printf("Event header:\n"); for (unsigned char* p=start; p!=start+48; ++p) printf("%i ", *p); printf("\n");
  return true;
}

// read one bank from start to maximally end_event, store output to &bank
bool read_LHCb_raw_bank(unsigned char* start, unsigned char* end_event, struct LHCb_Raw_Bank* bank) {
  ptrdiff_t n = end_event - start;
  if (n < 8) {
    fprintf(stderr, "not enough bytes for bank header: %ti (should be >=8)\n", n);
    return false;
  }
  if (start[0] != 0xcb ||
      start[1] != 0xcb) { // header should start from the magic pattern 0xcb 0xcb
    fprintf(stderr, "bank header does not start from 0xcb 0xcb: ");
    unsigned int show_nbytes = (0 <= n && n < 10) ? n : 10;
    for (unsigned char* i=start; i<start+show_nbytes; ++i) fprintf(stderr, "%i ", *i);
    fprintf(stderr, "... \n");
    return false;
  }
  unsigned int size = start[2] + (start[3] << 8);
  if (size > (ptrdiff_t)(n)) {
    fprintf(stderr, "decoded bank length (%i) is longer than the number of remaining bytes in event (%ti)\n",
            size, n);
    return false;
  }
  if (size < 8) {
    fprintf(stderr, "decoded bank length (%i) is shorter than the header length (8)\n", size);
    return false;
  }
  bank->start       = start + 8;
  bank->end         = start + size;
  bank->type        = start[4];
  bank->version     = start[5];
  bank->source_ID   = start[6] + (start[7] << 8);
  return true;
}

// open / memory map file to a bytestream file->start ... file->end
bool open_LHCb_raw_file_mmap(const char* file_name, struct LHCb_Raw_File* file) {
  int fd;
  struct stat statbuf;
  if ((fd  = open(file_name, O_RDONLY)) < 0) {
    perror("Cannot open file");
    return false;
  }
  if (fstat(fd, &statbuf) < 0) {
    perror("Cannot get file size");
    return false;
  }
  // printf("size is %llu\n", (unsigned long long)statbuf.st_size);
  void* file_start;
  if ((file_start = mmap(0, statbuf.st_size, PROT_READ, MAP_SHARED, fd, 0)) == MAP_FAILED) {
    perror("Cannot mmap");
    return false;
  }
  file->start =(unsigned char*) file_start;
  file->end = file->start + statbuf.st_size;
  return true;
}

bool process_LHCb_raw_file(const char* raw_file_name,
			   bool (*process_bank )(struct LHCb_Raw_Bank* , void*), void* bank_arg,
			   bool (*process_event)(struct LHCb_Raw_Event*, void*), void* event_arg,
			   unsigned int max_events,  // 0 means process all events
                           int debug_level) {
  struct LHCb_Raw_File file;
  struct LHCb_Raw_Bank bank;
  if (!open_LHCb_raw_file_mmap(raw_file_name, &file)) {
    fprintf(stderr, "Can not open file %s\n", raw_file_name);
    return false;
  }
  if (debug_level > 0) printf("File size = %li bytes\n", file.end - file.start);
  struct LHCb_Raw_Event event;
  unsigned char* next_event_start = file.start;
  do {
    if (!read_LHCb_raw_event(next_event_start, file.end, &event)) return false;
    //    print_LHCb_raw_event(&event);
    unsigned char* next_bank_start = event.start;
    do {
      if (!read_LHCb_raw_bank(next_bank_start, file.end, &bank)) return false;
      //      print_LHCb_raw_bank(&bank);
      if (process_bank != NULL) {
        if ((*process_bank)(&bank, bank_arg) == false) return false;
      }
      // if bank size is not a multiple of 4, the padding is made:
      // zeros are added in event stream until the next 32-bit boundary
      size_t n = (bank.end - bank.start) & 0x3;
      next_bank_start = bank.end + (n>0 ? 4-n : 0);
    } while (next_bank_start < event.end);
      if (process_event != NULL) {
        if ((*process_event)(&event, event_arg) == false) return false;
      }
      if (max_events > 0) {
        if (--max_events == 0) break;
      }
      // padding:
      size_t n = (event.end - event.start) & 0x3;
      next_event_start = event.end + (n>0 ? 4-n : 0);
  } while (next_event_start < file.end);
  return true;
}


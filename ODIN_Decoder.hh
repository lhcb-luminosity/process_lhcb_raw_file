#ifndef ODIN_Decoder_hh 
#define ODIN_Decoder_hh 1

#include <boost/dynamic_bitset.hpp>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <array>

struct ODIN_Decoder {
  // ODIN_Decoder::get(start, end) decodes ODIN bank contained in
  // [start...end) raw byte array. In case of error returns NULL, otherwise
  // const ODIN_Decoder::Fields* = pointer to array of 8 Fields having name()
  // and data() public methods. You can loop over these 8 fields like over any
  // C++ STL container:
  //
  // const ODIN_Decoder::Fields* odin;
  // if ((odin = get(start, end)) != NULL)
  //   for (auto& x : *odin) { printf("%s = %lu\n", x.name(), x.data()); }
  //
  // Alternatively, odin->run(), odin->event() etc. are also available below.
  //
  // Only version 7 is decoded (the most recent in Apr 2023) and only 8 fields
  // relevant for the luminosity measurement
  inline static const char field_names[] = "run,event,orbit,bcid,bx_type,gps,step,trig_type";
  //
  struct Field { // field decoding in ODIN bit stream
    const std::string& name() const { return m_name; }
    unsigned long int data() const { return m_data; }
  private:
    friend ODIN_Decoder;
    std::string m_name;
    unsigned int offset, size;
    //
    unsigned long int m_data; // not unsigned long long: limited by
    // boost::dynamic_bitset<unsigned char>::to_ulong, to_ulonglong is not
    // implemented in boost
  };
  enum {RUN=0, EVENT, ORBIT, BCID, BX_TYPE, GPS, STEP, TRIG_TYPE, N_FIELDS};
  struct Fields : public std::array<Field, N_FIELDS> {
    bool print(FILE* file) const; // prints comma-separated-values,
    // ends with last comma (so, can be directly followed by lumi-counters)
  };
  bool get(unsigned char* start, unsigned char* end, int debug_level = 0);
  //
  unsigned long int run()         const { return m_decoder[0].m_data; }
  unsigned long int event()       const { return m_decoder[1].m_data; }
  unsigned long int orbit()       const { return m_decoder[2].m_data; }
  unsigned long int bcid()        const { return m_decoder[3].m_data; }
  unsigned long int bx_type()     const { return m_decoder[4].m_data; }
  unsigned long int gps()         const { return m_decoder[5].m_data; }
  unsigned long int step()        const { return m_decoder[6].m_data; }
  unsigned long int trig_type()   const { return m_decoder[7].m_data; }
  //  unsigned long int trig_config() const { return m_decoder[8]; } // =0 in lumi-events, dropped
  ODIN_Decoder();
  const Fields& decoder() const { return m_decoder; }
private:
  Fields m_decoder;
  /* ODIN version 7:
     
     RunNumberSize                   = 32,
     RunNumberOffset                 = 0,
     EventTypeSize                   = 16,
     EventTypeOffset                 = 1 * 32 + 0,
     CalibrationStepSize             = 16,
     CalibrationStepOffset           = 1 * 32 + 16,
     GpsTimeSize                     = 64,
     GpsTimeOffset                   = 2 * 32,
     TriggerConfigurationKeySize     = 32,
     TriggerConfigurationKeyOffset   = 4 * 32,
     PartitionIDSize                 = 32,
     PartitionIDOffset               = 5 * 32,
     BunchIdSize                     = 12,
     BunchIdOffset                   = 6 * 32 + 0,
     BunchCrossingTypeSize           = 2,
     BunchCrossingTypeOffset         = 6 * 32 + 12,
     NonZeroSuppressionModeSize      = 1,
     NonZeroSuppressionModeOffset    = 6 * 32 + 14,
     TimeAlignmentEventCentralSize   = 1,
     TimeAlignmentEventCentralOffset = 6 * 32 + 15,
     TimeAlignmentEventIndexSize     = 6,
     TimeAlignmentEventIndexOffset   = 6 * 32 + 16,
     StepRunEnableSize               = 1,
     StepRunEnableOffset             = 6 * 32 + 22,
     TriggerTypeSize                 = 4,
     TriggerTypeOffset               = 6 * 32 + 23,
     TimeAlignmentEventFirstSize     = 1,
     TimeAlignmentEventFirstOffset   = 6 * 32 + 27,
     CalibrationTypeSize             = 4,
     CalibrationTypeOffset           = 6 * 32 + 28,
     OrbitNumberSize                 = 32,
     OrbitNumberOffset               = 7 * 32,
     EventNumberSize                 = 64,
     EventNumberOffset               = 8 * 32
  */
    /*
      const std::array<Field, N_FIELDS> m_decoder =
    {Field{   0,    32}, Field{8*32,   64}, Field{7*32,   32},
     Field{6*32,    12}, Field{6*32+12, 2}, Field{2*32,   64},
     Field{1*32+16, 16}, Field{6*32+23, 4}}; // for trig_config: , Field{4*32,   32}};
  */
  boost::dynamic_bitset<unsigned char> bits; // copy of ODIN, contains nBytes bytes,
  // allocated for the first event once and then reused since nBytes=const
};

// ============================================================================
// =                            Implementation                                =
// ============================================================================
inline ODIN_Decoder::ODIN_Decoder() {
  m_decoder[0].m_name = "run";       m_decoder[0].offset = 0;       m_decoder[0].size = 32;
  m_decoder[1].m_name = "event";     m_decoder[1].offset = 8*32;    m_decoder[1].size = 64;
  m_decoder[2].m_name = "orbit";     m_decoder[2].offset = 7*32;    m_decoder[2].size = 32;
  m_decoder[3].m_name = "bcid";      m_decoder[3].offset = 6*32;    m_decoder[3].size = 12;
  m_decoder[4].m_name = "bx_type";   m_decoder[4].offset = 6*32+12; m_decoder[4].size =  2;
  m_decoder[5].m_name = "gps";       m_decoder[5].offset = 2*32;    m_decoder[5].size = 64;
  m_decoder[6].m_name = "step";      m_decoder[6].offset = 1*32+16; m_decoder[6].size = 16;
  m_decoder[7].m_name = "trig_type"; m_decoder[7].offset = 6*32+23; m_decoder[7].size =  4;
  //    m_decoder[8].m_name = "trig_conf"; m_decoder[8].offset = 4*32;    m_decoder[8].size = 32;
}

inline bool ODIN_Decoder::get(unsigned char* start, unsigned char* end, int debug_level) {
  if (end - start != 40) {
    fprintf(stderr, "ODIN bank has %li bytes, not 40 required in ODIN format version 7. Exiting\n",
	    end - start);
    return false;
  }
  bits = boost::dynamic_bitset<unsigned char>(start, end);
  if (debug_level > 1) { // print bit stream, keep reverse order like when printing directly cout << bits
    for (int i=bits.size()-1; i>=0; --i) {
      if (i%8==7) std::cout << " " << i+1 << ":";
      std::cout << bits[i];
    }
    std::cout << '\n';
  }
  // Decoding part
  {
    size_t bits_size = bits.size();
    for (Fields::iterator it=m_decoder.begin(); it!=m_decoder.end(); ++it) {
      unsigned int n_strip = bits_size - it->size; // = right offset + (left) offset
      // n_strip - offset         offset
      // ................ counter ......
      // shift left by n_strip - offset:
      // counter ......00000000000000000
      // then right by n_strip:
      // 00000000000000000000000 counter
      it->m_data = (unsigned long int) ((bits << (n_strip - it->offset)) >> n_strip).to_ulong();
    }
    if (debug_level > 0) {
      for (std::array<Field, 8>::const_iterator it=m_decoder.begin(); it!=m_decoder.end(); ++it)
        if (printf("%s = %lu\n", it->m_name.c_str(), it->m_data) <= 0) return false;
    }
    return true;
  }
}

inline bool ODIN_Decoder::Fields::print(FILE* file) const {
  for (const_iterator it=begin(); it!=end(); ++it)
    if (fprintf(file, "%lu,", it->m_data) <= 0) return false;
  return true;
}
  
#endif



# process\_LHCb\_raw\_file

Allows to read and process Run 3 LHCb files with online format (eg. \*.raw or
\*.mdf) without any external dependencies, in particular independently of LHCb
software.

Currently, used for decoding of HltLumiSummary banks which have a relatively
simple format.


## Usage

Clone this repository and run make. This will create "lumi2csv" executable
which can be run as

    lumi2csv [-n <max_nevents>] ([-f <file_with_input_files>] | [<input1> [<input2> [<input3>]]...]) <output_file_prefix>

Input files are either read from <file\_with\_input\_files> or, if it is not
given, from the command line.  The last name in the command line defines the
output file names of the form <output\_file\_prefix>\_xxxxxxxx.bz2, where
xxxxxxxx are substituted by 4 bytes of the encoding schema.  Any number of
schemas can be present in one file, eg. two lumi-lines produce two output
files. -n max\_nevents instructs the program to stop after the given number of
events.

For debugging, the binary content of the file can be printed with: 

    dump_all_events <file_name> | less


## The raw Run 3 format is taken from [EDMS note](https://edms.cern.ch/ui/#!master/navigator/document?P:1602123437:101144907:subDocs)

\*.raw file is mapped to memory (using mmap) and split into
events/banks in process\_LHCb\_raw\_file.{h,c} written in C. The user
can supply process\_event() and process\_bank() functions which will be
called per event and bank, respectively.


## The banks are distinguished by their types defined in [LHCb Event/RawBank.h](https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/DAQEvent/include/Event/RawBank.h#L62)

and duplicated in const char\* LHCb\_raw\_bank\_name().


## Encoding schema of HltLumiSummary

The first 4 bytes in this bank determine the positions of
lumi-counters in the bank bit stream, according to the JSON files in
[GitLab CondDB lumi\_counters/json](https://gitlab.cern.ch/lhcb-conddb/file-content-metadata/-/tree/master/luminosity_counters/json) directory which are downloaded using
"curl" library when needed and stored locally. The decoding is done in
HltLumiSummary\_Decoder.hh. After that
**`HltLumiSummary_Decoder::counter()`** returns a C++ **`stl::map<string,
unsigned long int>`** with values of all lumi-counters. Namely, its (key,
value) pairs contain (counter name, value). Eg. in C++17 (with -std=c++17
flag in g++) or above one can loop over them as

    for (const auto& [key, value] : decoder.counter()) cout << key << ' ' << value << '\n';

The JSON format is interpreted by [GitHub nlohmann/json](https://github.com/nlohmann/json) C++ library, copied to
nlohmann\_json.hpp file. The bit stream decoding is performed using
[boost::dynamic\_bitset](https://www.boost.org/doc/libs/1_81_0/libs/dynamic_bitset/dynamic_bitset.html) C++ class.  At lxplus or other machines (eg. in grid)
where software packages are distributed via `/cvmfs,` the boost headers are
taken from one of LCG Boost releases, namely, from
`/cvmfs/sft.cern.ch/lcg/releases/LCG_103/Boost/` as specified in
`Makefile`. It does not matter which machine architecture to use since Boost
is template-based. Any recent Boost version should work: the last change in
dynamic\_bitset<> was in Boost 1.56.0 while the chosen in `Makefile` LCG
release has version 1.81.0.  For compiling at other machines, Boost C++ should
be installed centrally or one can explicitly add -I<path\_to\_boost> to g++
&#x2026; command in `Makefile`.

In addition, the code uses C++17 standard which is supported by g++ from major
version 8. In CentOS7 (eg. lxplus7.cern.ch) the default version is old
(v4.8). As a workaround, `Makefile` internally checks the major g++ version, and
for versions <8 calls
`/cvmfs/lhcb.cern.ch/lib/lcg/releases/gcc/12.1.0/$(ARCH)-centos7/setup.sh` which
switches to gcc 12.1.0.

lumi2csv.C shows how to loop over events and banks and how to use
HltLumiSummary\_Decoder.hh.

